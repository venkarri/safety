from xchurn import (
    delta_calculator,
    get_logger,
    list_dir,
    get_s3_client,
    fetch_filename_stem
)
import ntpath
import os

logger = get_logger(__file__)


def main():
    bucket_name = 'nexscope-lambda'
    page_limit = 1000
    s3_client = get_s3_client(True)
    pubmed_s3_root = 'data/2018/04/04/structured/processed/pubmed'
    pubmed_s3_decompressed = os.path.join(pubmed_s3_root, 'decompressed')
    pubmed_s3_deltacon = os.path.join(pubmed_s3_root, 'deltacon')
    root_dir = os.path.join(os.path.expanduser('~'), 'medmeme')
    data_dir = os.path.join(root_dir, 'data')
    s3_downloads_dir = os.path.join(data_dir, 's3downloads')
    pubmed_files_list = os.path.join(s3_downloads_dir,
                                     'pubmed_decompressed_0404')

    with open(pubmed_files_list, 'r') as f:
        for idx, line in enumerate(f.readlines()):
            filename_stem = fetch_filename_stem(ntpath.basename(line))
            if filename_stem == 'pubmed18n0091':
                break
            decompressed_s3_dir = os.path.join(
                pubmed_s3_decompressed, filename_stem)
            deltacon_s3_dir = os.path.join(
                pubmed_s3_deltacon, filename_stem)
            decompressed_files_list = os.path.join(
                s3_downloads_dir,
                '{}.decompressed'.format(filename_stem))
            deltacon_files_list = os.path.join(
                s3_downloads_dir,
                '{}.deltacon'.format(filename_stem))
            diff_list = os.path.join(
                s3_downloads_dir,
                '{}.diff'.format(filename_stem))
            logger.debug('Counting {}'.format(decompressed_s3_dir))
            with open(decompressed_files_list, 'w') as df:
                decompressed_count = list_dir(
                    s3_client,
                    bucket_name,
                    decompressed_s3_dir,
                    page_limit,
                    pages=None,
                    delimiter='',
                    file_writer=df,
                    write_file_name_only=True,
                    compare_hash=False,
                    count_only=True)
            with open(deltacon_files_list, 'w') as df:
                deltacon_count = list_dir(
                    s3_client,
                    bucket_name,
                    deltacon_s3_dir,
                    page_limit,
                    pages=None,
                    delimiter='',
                    file_writer=df,
                    write_file_name_only=True,
                    compare_hash=False,
                    count_only=True)
            logger.debug('{} decompressed count: {}'.format(
                filename_stem, decompressed_count))
            logger.debug('{} deltacon count: {}'.format(
                filename_stem, deltacon_count))
            diff_count = decompressed_count - deltacon_count
            logger.debug('Diff: {}'.format(abs(
                diff_count)))
            if diff_count:
                delta_calculator(
                    decompressed_files_list,
                    deltacon_files_list,
                    diff_list
                )
            else:
                os.remove(decompressed_files_list)
                os.remove(deltacon_files_list)


if __name__ == '__main__':
    main()
