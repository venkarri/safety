from boilerpipe.extract import Extractor
from collections import defaultdict
from lshared import (
    s3_download_file,
    get_s3_client,
    s3_exists
)
import rapidjson
import xmltodict
import tempfile
import logging
import codecs
import re
import os


logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
temp_dir = '/tmp'

source_conversion_map = {
    'pubmed': True,
    'cochrane': True,
    'who': True,
    'drugbank': False,
    'dailymed': True,
    'nct': True,
    'aps': True
}
json_modifier_map = {
    'pubmed': False,
    'cochrane': False,
    'who': True,
    'drugbank': False,
    'dailymed': False,
    'nct': False,
    'aps': False
}


def strategy1(input_file, output_file):
    with open(output_file, 'w', encoding='utf-8') as fo:
        with codecs.open(input_file, 'rb') as fi:
            rapidjson.dump(xmltodict.parse(
                fi,
                attr_prefix='',
                cdata_key='text'), fo)
            logger.debug('NOT using dict_constructor')


def strategy2(input_file, output_file):
    data = dict()
    compilation = re.compile(b'^([A-Z]+)(:\s)(.*)(\s*)$')
    with open(output_file, 'w', encoding='utf-8') as fo:
        with open(input_file, 'rb') as fi:
            for line in fi:
                line = line.decode('latin-1').encode('utf-8')
                matches = re.findall(compilation, line)
                if matches:
                    (key, value) = (matches[0][0].decode('utf-8'),
                                    matches[0][2].decode(
                        'utf-8').replace('\r', ''))
                    if key in data:
                        if isinstance(data[key], list):
                            data[key].append(value)
                        else:
                            data[key] = [data[key], value]
                    else:
                        data[key] = value
        rapidjson.dump(data, fo, ensure_ascii=False)


def strategy3(input_file, output_file):
    with open(output_file, 'w') as fo:
        with open(input_file) as fi:
            extractor = Extractor(
                extractor='ArticleExtractor', html=fi.read())
            fo.write(extractor.getText())


def strategy4(input_file, output_file):
    with open(output_file, 'w', encoding='utf-8') as fo:
        with codecs.open(input_file, 'rb') as fi:
            rapidjson.dump(xmltodict.parse(
                fi,
                dict_constructor=lambda *args, **kwargs: defaultdict(
                    list, *args, **kwargs),
                attr_prefix='',
                cdata_key='text'), fo)
            logger.debug('Using dict_constructor')


source_strategy_map = {
    'pubmed': 'strategy1',
    'cochrane': 'strategy2',
    'who': 'strategy1',
    'dailymed': 'strategy1',
    'nct': 'strategy1',
    'aps': 'strategy3'
}

strategy_map = {
    'strategy1': strategy1,
    'strategy2': strategy2,
    'strategy3': strategy3,
    'strategy4': strategy4
}


def lambda_convert(
        source_name,
        bucket_name,
        infile_basename,
        outfile_basename,
        s3_in_file,
        s3_out_file,
        force_convert=False,
        strategy_name=None):
    s3_client = get_s3_client()
    if not force_convert and s3_exists(
            s3_client,
            bucket_name,
            s3_out_file):
        logger.debug('Skipping deltacon of {} because its JSON '
                     'counterpart already exists at {}'.format(
                         s3_in_file,
                         s3_out_file))

    local_out_json_file = os.path.join(temp_dir, outfile_basename)
    local_in_file = s3_download_file(s3_in_file, bucket_name, temp_dir)
    logger.debug('Downloaded {} to {}'.format(s3_in_file, local_in_file))

    if not source_conversion_map[source_name]:
        s3_client.upload_file(
            local_in_file,
            bucket_name,
            s3_out_file
        )
        logger.debug('Successfully copied {} to {} because no'
                     ' conversion is required'.format(
                         s3_in_file,
                         s3_out_file))
        return

    if not strategy_name:
        strategy_name = source_strategy_map[source_name]

    strategy = strategy_map[strategy_name]

    logger.debug('Using {}'.format(strategy_name))

    try:
        logger.debug(
            'Converting {}...'.format(
                local_in_file))
        strategy(
            local_in_file,
            local_out_json_file)
    except Exception as e:
        logger.debug(
            'Failed converting {} due to\n{}'.format(
                s3_in_file, e))
        return

    if json_modifier_map[source_name]:
        json_modifier(local_out_json_file)

    s3_client.upload_file(
        local_out_json_file,
        bucket_name,
        s3_out_file
    )

    os.remove(local_in_file)
    os.remove(local_out_json_file)

    logger.debug('Successfully converted {} '
                 '(locally at {}) to {} and uploaded it '
                 'to {}'.format(
                     s3_in_file,
                     local_in_file,
                     local_out_json_file,
                     s3_out_file))


def json_modifier(json_file):
    with open(json_file, 'r') as jf:
        content = rapidjson.load(jf)
        content['id'] = [item for item in content[
            'doc']['str'] if item['name'] == 'id'][0]['text']

    with open(json_file, 'w', encoding='UTF-8') as fo:
        rapidjson.dump(content, fo)


def lambda_handler(event, context):
    logger.debug(event)
    for record in event['Records']:
        if 'EventSource' in record and record['EventSource'] == 'aws:sns':
            payload = rapidjson.loads(record['Sns']['Message'])
            source_name = payload['source_name']
            bucket_name = payload['bucket_name']
            infile_basename = payload['infile_basename']
            outfile_basename = payload['outfile_basename']
            s3_in_file = payload['s3_in_file']
            s3_out_file = payload['s3_out_file']
            force_convert = payload['force_convert']
            strategy_name = payload['strategy_name']

            break
        elif 'eventSource' in record and record['eventSource'] == 'aws:s3':
            payload = record['s3']
            source_name = 'pubmed'
            bucket_name = payload['bucket']['name']
            s3_in_file = payload['object']['key']
            infile_basename = ntpath.basename(s3_in_file)
            outfile_basename = infile_basename.replace('.xml', '.json')
            s3_out_file = s3_in_file.replace('.xml', '.json').replace(
                'decompressed', 'deltacon')
            force_convert = True

    lambda_convert(
        source_name,
        bucket_name,
        infile_basename,
        outfile_basename,
        s3_in_file,
        s3_out_file,
        force_convert,
        strategy_name
    )
