from xchurn import s3_download_file, bucket_1
import os

def main():
	root_dir = os.path.expanduser('~')
	data_dir = os.path.join(root_dir, 'medmeme', 'data', 'pubmed')

	if not os.path.exists(data_dir):
		os.makedirs(data_dir)

	keys = [
		# 'data/2018/04/09/structured/processed/dailymed/decompressed/001229b5-d94e-403b-b145-f94a6d13354c.xml',
		# 'data/2018/04/09/structured/processed/dailymed/decompressed/0013824B-6AEE-4DA4-AFFD-35BC6BF19D91.xml',
		# 'data/2018/04/09/structured/processed/dailymed/decompressed/001447cf-1dad-47bf-be69-f41d581dd21d.xml',
		# 'data/2018/04/09/structured/processed/dailymed/decompressed/001568df-2242-4709-ac6e-4d36e2ed1745.xml',
		# 'data/2018/04/09/structured/processed/dailymed/decompressed/001714c1-a292-40be-bfbd-0ff67193ace6.xml',
		# 'data/2018/05/09/structured/processed/who/decompressed/who_article_afr-10001.xml',
		# 'data/2018/05/09/structured/processed/who/decompressed/who_article_biblio-2016.xml',
		# 'data/2018/05/09/structured/processed/who/decompressed/who_article_oai-imsear.hellis.org-123456789-21595.xml',
		# 'data/2018/05/09/structured/processed/who/decompressed/who_article_who-37254.xml',
		# 'data/2018/05/09/structured/processed/who/decompressed/who_article_wprim-248620.xml',
		'data/2018/04/11/structured/processed/pubmed/decompressed/pubmed_article_9925023.xml',
		# 'logs/2018/04/09/structured/nct/decompression_20180425042444.log',
		# 'logs/2018/04/09/structured/nct/deltacon_20180425042444.log',
		# 'logs/2018/04/10/structured/nct/decompression_20180427194505.log',
		# 'logs/2018/04/10/structured/nct/deltacon_20180429215724.log'
	]

	for key in keys:
		bucket_name = bucket_1
		# bucket_name = 'nexscope-safety'
		s3_download_file(key, bucket_name, data_dir)
	# s3_download_file(key4, bucket_1, data_dir)

if __name__ == '__main__':
	main()
